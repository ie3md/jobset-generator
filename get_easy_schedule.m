function [LP_X, LP_s, LP_time] = get_easy_schedule(J)
%GET_EASY_SCHEDULE Summary of this function goes here
%   Detailed explanation goes here
%% 

N = size(J, 1);

%%
% Time intervals
TT=zeros(1,2*N); 
T=zeros(1,2); % T will not be changed, T(1)=0, T(k)=latest deadline
for i=1:N
    TT(i)=J(i,1);
    TT(N+i)=J(i,2);
end
TT=sort(TT); % T will not be changed
t=1;
T(1)=TT(1);
for i=2:2*N
    if TT(i)> T(t)
        t=t+1;
        T(t)=TT(i);
    end
end
k=length(T)-1;


%% easy LP
tic;
VarNum=N*k+1; % total variable number
A=zeros(2,VarNum); %should be more than k
b=zeros(2,1); %at least 3N
% Ax<=b
t=0; %constraints counter

%(1)
for i=1:N
    t=t+1;
    for j=1:k
        if J(i,1)<=T(j) && T(j+1)<=J(i,2)
            A(t,(i-1)*k+j)=-1;
        end
    end
    b(t)=-J(i,3);
end

%(2)
for j=1:k
    t=t+1;
    for i=1:N
        A(t,(i-1)*k+j)=1;
    end
    b(t)=T(j+1)-T(j);
end

%(3)
for m=2:k+1
    for l=1:m-1
        t=t+1;
        for i=1:N
            if J(i,4)==1 && J(i,2)<=T(m) 
                for j=l:m
                    A(t,(i-1)*k+j)=1;
                end
            end
        end
        A(t,VarNum)=T(l)-T(m);
        b(t)=0;
    end
end


% lower and upper bounds
lp = zeros(VarNum,1);
up = T(k+1)*ones(VarNum,1);
up(VarNum)=1;
for i=1:N
    for j=1:k
        if J(i,1)>T(j)
            up((i-1)*k+j)=0;
        end
        if J(i,2)<=T(j)
            up((i-1)*k+j)=0;
        end
    end
end
% f
f=zeros(VarNum,1); f(VarNum)=1;

%optimize
x = linprog(f,A,b,[],[],lp,up); % x (variables) - N*(2N-1):x_ij, N*(2N-1)^2: y_ij^(l), s
x = round(100*x)*0.01;

% convert results
LP_X=zeros(N,k);
LP_s = inf;

if ~isempty(x)
    for i=1:N
        for j=1:k
            LP_X(i,j)=x((i-1)*k+j);
        end
    end
    LP_s=x(VarNum);
end

LP_time=toc;


end

