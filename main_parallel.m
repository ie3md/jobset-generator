%% clear
fclose all; clear; clc;

%% parameters
task_id = getenv('SLURM_ARRAY_TASK_ID');

data_dir = ['data-', task_id];
file_fmt = 'jobset-%04d.csv';
result_dir = ['results-', task_id];
result_fmt = 'result-%04d.mat';
num_jobs = 100;
output_file = sprintf('output-%s.log', task_id);

%% create output directories
mkdir(data_dir);
mkdir(result_dir);

%% open output file

fout = fopen(output_file, 'w');

%% run loop

for ix = 1:num_jobs
    %% seed the random number generator
    seed = mod(int64((now * 1e8)), 1e8);
    rng(seed);
    
    %% read or generate the data
    filename = sprintf([data_dir, '/', file_fmt], ix);
    if exist(filename, 'file') == 2
        J = csvread(sprintf([data_dir, '/', file_fmt], ix));
        generated = false;
    else
        J= MC_job_creator_R (20, 0.8, 0.5, 5); 
        J(:,1:2) = round(J(:,1:2)*10);
        J(:,3) = round(J(:,3)*10+0.5);
        generated = true;
    end        
     
    %% test the data
    [LPEasy_X, LPEasy_s, LPEasy_time] = get_easy_schedule(J);
    
    while LPEasy_s >= 1
        J= MC_job_creator_R (20, 0.8, 0.5, 5); 
        J(:,1:2) = round(J(:,1:2)*10);
        J(:,3) = round(J(:,3)*10+0.5);
        
        [LPEasy_X, LPEasy_s, LPEasy_time] = get_easy_schedule(J);
    end
    
    csvwrite(sprintf([data_dir, '/', file_fmt], ix), J);

    [LP_X, LP_s, LP_time] = get_critical_schedule(J);

    save(sprintf([result_dir, '/', result_fmt], ix), 'J', ...
        'LPEasy_X', 'LPEasy_s', 'LPEasy_time', ...
        'LP_X', 'LP_s', 'LP_time', 'seed');
    fprintf('%04d\t%.4f\t%.4f\t%d\t%d\n', ix, ...
        LPEasy_s, LP_s, seed, generated);
    fprintf(fout, '%03d\t%.4f\t%.4f\t%d\t%d\n', ix, ...
        LPEasy_s, LP_s, seed, generated);
end

%% close output file

fclose(fout);
