%% clear
fclose all; clear; clc;

%% parameters
jobset_size = 10;
jobset_loadrate = 0.8;
jobset_hilorate = 0.5;
jobset_alpha = 5;

base_dir = '.';
data_dir = sprintf('%s/%d-jobs/data', base_dir, jobset_size);
result_dir = sprintf('%s/%d-jobs/results', base_dir, jobset_size);
output_file = sprintf('%s/%d-jobs/output.log', base_dir, jobset_size);

file_fmt = 'jobset-%03d.csv';
result_fmt = 'result-%03d.mat';
data_fmt = 'data-%s-%03d.csv';

num_jobs = 10;

header_fmt = '%s\t%s\t%s\t%-8s\t%-4s\t%-8s\t%-8s\n';
row_fmt = '%d\t%5g\t%5g\t%-8d\t%4d\t%-8g\t%-8g\n';
field_names = {'ix', 's_LP1', 's_LP2', 'seed', 'fail', 't_LP1', 't_LP2'};

%% create output directories
[~, ~, ~] = mkdir(base_dir, data_dir);
[~, ~, ~] = mkdir(base_dir, result_dir);

%% open output file

fout = fopen(output_file, 'w');

%% print header
fprintf(header_fmt, field_names{:});
fprintf(fout, header_fmt, field_names{:});

%% run loop
for ix = 1:num_jobs
    %% read or generate the data
    filename = sprintf([data_dir, '/', file_fmt], ix);
    if exist(filename, 'file') == 2
        J = csvread(sprintf([data_dir, '/', file_fmt], ix));
        generated = false;
        seed = false;
    else
        %% seed the random number generator
        seed = mod(int64((now * 1e8)), 1e8);
        rng(seed);
        %% generate the job file
        J = MC_job_creator_R(jobset_size, jobset_loadrate, jobset_hilorate, jobset_alpha); 
        J(:,1:2) = round(J(:,1:2)*10);
        J(:,3) = round(J(:,3)*10+0.5);
        generated = true;
    end        
     
    %% test the data
    [LPEasy_X, LPEasy_s, LPEasy_time] = get_easy_schedule(J);
    failures = 0;
    
    while LPEasy_s >= 1
        failures = failures + 1;
        fprintf('s = %d, failures = %d\n', LPEasy_s, failures);
        J = MC_job_creator_R(jobset_size, jobset_loadrate, jobset_hilorate, jobset_alpha); 
        J(:,1:2) = round(J(:,1:2)*10);
        J(:,3) = round(J(:,3)*10+0.5);
        generated = true;
        
        [LPEasy_X, LPEasy_s, LPEasy_time] = get_easy_schedule(J);
    end
    
    csvwrite(sprintf([data_dir, '/', file_fmt], ix), J);

    [LP_X, LP_s, LP_time] = get_critical_schedule(J);

    parsave(sprintf([result_dir, '/', result_fmt], ix), ...
        struct(...
        'J', J, ...
        'LPEasy_X', LPEasy_X, ...
        'LPEasy_s', LPEasy_s, ...
        'LPEasy_time', LPEasy_time, ...
        'LP_X', LP_X, ...
        'LP_s', LP_s, ...
        'LP_time', LP_time, ...
        'seed', seed...
    ));

    csvwrite(sprintf([result_dir, '/', data_fmt], 'LP1X', ix), LPEasy_X);
    csvwrite(sprintf([result_dir, '/', data_fmt], 'LP2X', ix), LP_X);
    
    fprintf(row_fmt, ix, LPEasy_s, LP_s, seed, failures, LPEasy_time, LP_time);
    fprintf(fout, row_fmt, ix, LPEasy_s, LP_s, seed, failures, LPEasy_time, LP_time);
end

%% close output file

fclose(fout);
