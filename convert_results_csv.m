%% clear
fclose all; clear; clc;

%% parameters
results_dir = 'results';
output_dir = 'output/csv';
data_fmt = 'data-%s-%03d.csv';

%% get list of mat files
mat_files = dir([results_dir, '/*.mat']);

%% create output dir
if ~exist(output_dir, 'dir')
    mkdir(output_dir);
end

%% initialize variables
num_files = size(mat_files, 1);

%% run loop
for ix = 1:num_files
    %% load the data
    data = load(sprintf('%s/%s', results_dir, mat_files(ix).name));
    
    %%
    csvwrite(sprintf([output_dir, '/', data_fmt], 'LP1X', ix), data.LPEasy_X);
    csvwrite(sprintf([output_dir, '/', data_fmt], 'LP2X', ix), data.LP_X);
    
    fprintf('%03d\t%.4f\t%.4f\t%d\n', ix, ...
        data.LPEasy_s, data.LP_s, data.seed);
end
