mkdir('data');

for i = 1:100
    
    N=20; % Job number
    J= MC_job_creator_R (N, 0.8, 0.5, 5); %Job set, [a,d,c,phi]
    J(:,1:2) = round(J(:,1:2)*10);
    J(:,3) = round(J(:,3)*10+0.5); %Int
    
    csvwrite(sprintf('data/jobset-%03d.csv', i), J);
    
end
